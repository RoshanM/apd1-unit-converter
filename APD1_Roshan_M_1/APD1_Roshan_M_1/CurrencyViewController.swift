//  CurrencyViewController.swift
//  APD1_Roshan_M_1
//
//  Created by Roshan Mykoo on 11/6/15.
//  Copyright © 2015 Roshan Mykoo. All rights reserved.
//

import UIKit
import Alamofire



class CurrencyViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate  {
    @IBOutlet weak var currencyPicker: UIPickerView!
    @IBOutlet weak var numberInput: UITextField!
    @IBOutlet weak var conversionResult: UITextView!
    var numString : Double = 0.0
    var currencyNames = [];
    var currencyRates = [Double]();
    let pickerData = [];
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Dissmiss Keyboard Function
        let bye: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(bye)
        
        Alamofire.request(.GET, "http://apilayer.net/api/live?access_key=d1c355d9ebdbf1d72afeac22bae41695").responseJSON { response in
            //print(response.request)
            //print(response.response)
            //print(response.data)
            //print(response.result)
            //debugPrint(response)
            
            //setting the data from API
            if let JSON = response.result.value {
                //print("JSON: \(JSON)")
                let dictionary = JSON as! NSDictionary
                //print(dictionary.valueForKey("quotes"))
                
                //setting API data from dictionry into seprate arrays, One for Keys and one for values
                let quotes = dictionary.valueForKey("quotes") as! NSDictionary
                self.currencyNames = quotes.allKeys;
                self.currencyRates = quotes.allValues as! [Double];
                //print(self.currencyNames)
                //print(self.currencyRates)
            
                dispatch_async(dispatch_get_main_queue()) {
                 self.currencyPicker.reloadAllComponents()
                }
            }
        }
        currencyPicker.delegate = self;
        currencyPicker.dataSource = self;
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //Dismiss Keyboard Function
    func dismissKeyboard() {
        view.endEditing(true);
    }
   //Clear Button Function
    @IBAction func clearButton(sender: AnyObject) {
        conversionResult.text = nil;
        numberInput.text = nil;
    }
    
    
    //MARK: - Delegates and data sources
    //MARK: Data Sources
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return currencyNames.count
    }
    //MARK: Delegates
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return currencyNames[row] as? String;
    }
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        //Converting String from Text Field into double for conversion
        if let newNumb =  Double(numberInput.text!) {
            let roundingNumber = newNumb * currencyRates[row]
            //Rounding the resuult for a proper Dollar and Cent amount.
            let y = round(100.0 * roundingNumber) / 100.0
            conversionResult.text = "\(y)"
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
