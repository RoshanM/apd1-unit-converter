//
//  LengthViewController.swift
//  APD1_Roshan_M_1
//
//  Created by Roshan Mykoo on 11/6/15.
//  Copyright © 2015 Roshan Mykoo. All rights reserved.
//

import UIKit

class LengthViewController: UIViewController {
    @IBOutlet weak var originalLength: UISegmentedControl!
    @IBOutlet weak var convertLength: UISegmentedControl!
    @IBOutlet weak var numberInput: UITextField!
    var numString : Double = 0.0
    
    @IBOutlet weak var displayResult: UITextField!
    @IBOutlet weak var convertionResult: UITextView!


    override func viewDidLoad() {
        super.viewDidLoad()
          // Do any additional setup after loading the view.
        
        //Setting the Second segmented control to the second option to prevent a same conversion error from starting when the app initally loads
        convertLength.selectedSegmentIndex = 1;

        
        //Tap gesture to dissmiss the keyboard.
        let bye: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(bye)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    //Dismiss Keyboard Function
    func dismissKeyboard() {
        view.endEditing(true);
    }
    
    
    //Function for the convert button
    @IBAction func convertButton(sender: AnyObject) {
        //if let statement to convert the string from the text field into a double to start the conversions
        if let newNumb =  Double(numberInput.text!) {
            self.numString = newNumb;
        }
            //Convert Inches to Ft
            if originalLength.selectedSegmentIndex == 0 && convertLength.selectedSegmentIndex == 1{
                let result = numString * 0.0833333;
                convertionResult.text = "\(result) Feet"
            }
            //Convert Inches to yrd
            if originalLength.selectedSegmentIndex == 0 && convertLength.selectedSegmentIndex == 2{
                let result = numString * 0.0277778;
                convertionResult.text = "\(result) Yards"
            }
            //Convert Inches to Mi
            if originalLength.selectedSegmentIndex == 0 && convertLength.selectedSegmentIndex == 3{
                let result = numString * 1.5783e-5;
                convertionResult.text = "\(result) Miles"
            }
            //Convert Inches to mm
            if originalLength.selectedSegmentIndex == 0 && convertLength.selectedSegmentIndex == 4{
                let result = numString * 25.4;
                convertionResult.text = "\(result) Milimeters"
            }
            //Convert Inches to cm
            if originalLength.selectedSegmentIndex == 0 && convertLength.selectedSegmentIndex == 5{
                let result = numString * 2.54;
                convertionResult.text = "\(result) Centimeters"
            }
            //Convert Inches to M
            if originalLength.selectedSegmentIndex == 0 && convertLength.selectedSegmentIndex == 6{
                let result = numString * 0.0254;
                convertionResult.text = "\(result) Meters"
            }
            //Convert Inches to Km
            if originalLength.selectedSegmentIndex == 0 && convertLength.selectedSegmentIndex == 7{
                let result = numString * 2.54e-5;
                convertionResult.text = "\(result) Kilometers"
            }
            //ERROR Cannot convert Inches to Inches
            if originalLength.selectedSegmentIndex == 0 && convertLength.selectedSegmentIndex == 0{
                let alert = UIAlertController(title: "ERROR", message: "Cannot Convert Inches to Inches", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
            }
            
            
            //Convert Feet to In
            if originalLength.selectedSegmentIndex == 1 && convertLength.selectedSegmentIndex == 0{
                let result = numString * 12;
                convertionResult.text = "\(result) Inches"
            }
            //Convert Feet to Yrd
            if originalLength.selectedSegmentIndex == 1 && convertLength.selectedSegmentIndex == 2{
                let result = numString * 0.333333;
                convertionResult.text = "\(result) Yards"
            }
            //Convert Feet to Mi
            if originalLength.selectedSegmentIndex == 1 && convertLength.selectedSegmentIndex == 3{
                let result = numString * 0.000189394;
                convertionResult.text = "\(result) Miles"
            }
            //Convert Feet to mm
            if originalLength.selectedSegmentIndex == 1 && convertLength.selectedSegmentIndex == 4{
                let result = numString * 304.8;
                convertionResult.text = "\(result) Milimeters"
            }
            //Convert Feet to cm
            if originalLength.selectedSegmentIndex == 1 && convertLength.selectedSegmentIndex == 5{
                let result = numString * 30.48;
                convertionResult.text = "\(result) Centimeters"
            }
            //Convert Feet to M
            if originalLength.selectedSegmentIndex == 1 && convertLength.selectedSegmentIndex == 6{
                let result = numString * 0.3048;
                convertionResult.text = "\(result) Meters"
            }
            //Convert Feet to Km
            if originalLength.selectedSegmentIndex == 1 && convertLength.selectedSegmentIndex == 7{
                let result = numString * 0.0003048;
                convertionResult.text = "\(result) Kilometers"
            }
            //ERROR Cannot convert feet to feet
            if originalLength.selectedSegmentIndex == 1 && convertLength.selectedSegmentIndex == 1{
                let alert = UIAlertController(title: "ERROR", message: "Cannot Convert Feet to Feet ", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
            }
            
            
            //Convert Yard to In
            if originalLength.selectedSegmentIndex == 2 && convertLength.selectedSegmentIndex == 0{
                let result = numString * 36;
                convertionResult.text = "\(result) Inches"
            }
            //Convert Yard to Ft
            if originalLength.selectedSegmentIndex == 2 && convertLength.selectedSegmentIndex == 1{
                let result = numString * 3;
                convertionResult.text = "\(result) Feet"
            }
            //Convert Yard to MI
            if originalLength.selectedSegmentIndex == 2 && convertLength.selectedSegmentIndex == 3{
                let result = numString * 0.000568182;
                convertionResult.text = "\(result) Miles"
            }
            //Convert Yard to mm
            if originalLength.selectedSegmentIndex == 2 && convertLength.selectedSegmentIndex == 4{
                let result = numString * 914.4;
                convertionResult.text = "\(result) Milimeters"
            }
            //Convert Yard to cm
            if originalLength.selectedSegmentIndex == 2 && convertLength.selectedSegmentIndex == 5{
                let result = numString * 91.44;
                convertionResult.text = "\(result) Centimeters"
            }
            //Convert Yard to M
            if originalLength.selectedSegmentIndex == 2 && convertLength.selectedSegmentIndex == 6{
                let result = numString * 0.9144;
                convertionResult.text = "\(result) Meters"
            }
            //Convert Yard to Km
            if originalLength.selectedSegmentIndex == 2 && convertLength.selectedSegmentIndex == 7{
                let result = numString * 0.0009144;
                convertionResult.text = "\(result) Kilometers"
            }
            //ERROR cannot convert Yard to Yard
            if originalLength.selectedSegmentIndex == 2 && convertLength.selectedSegmentIndex == 2{
                let alert = UIAlertController(title: "ERROR", message: "Cannot Convert Yards to Yards ", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
            }
            
            
            //Convert Miles to In
            if originalLength.selectedSegmentIndex == 3 && convertLength.selectedSegmentIndex == 0{
                let result = numString * 63360;
                convertionResult.text = "\(result) Inches"
            }
            //Convert Miles to Ft
            if originalLength.selectedSegmentIndex == 3 && convertLength.selectedSegmentIndex == 1{
                let result = numString * 5280;
                convertionResult.text = "\(result) Feet"
            }
            //Convert Miles to Yrd
            if originalLength.selectedSegmentIndex == 3 && convertLength.selectedSegmentIndex == 2{
                let result = numString * 1760;
                convertionResult.text = "\(result) Yards"
            }
            //Convert Miles to mm
            if originalLength.selectedSegmentIndex == 3 && convertLength.selectedSegmentIndex == 3{
                let result = numString * 1.609e+6;
                convertionResult.text = "\(result) Milimeters"
            }
            //Convert Miles to cm
            if originalLength.selectedSegmentIndex == 3 && convertLength.selectedSegmentIndex == 4{
                let result = numString * 160934;
                convertionResult.text = "\(result) Centimeters"
            }
            //Convert Miles to M
            if originalLength.selectedSegmentIndex == 3 && convertLength.selectedSegmentIndex == 5{
                let result = numString * 1609.34;
                convertionResult.text = "\(result) Meters"
            }
            //Convert Miles to Km
            if originalLength.selectedSegmentIndex == 3 && convertLength.selectedSegmentIndex == 6{
                let result = numString * 1.60934;
                convertionResult.text = "\(result) Kilometers"
            }
            //ERROR Cannot convert Miles to Miles
            if originalLength.selectedSegmentIndex == 3 && convertLength.selectedSegmentIndex == 3{
                let alert = UIAlertController(title: "ERROR", message: "Cannot Convert Miles to Miles ", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
            }
            
            
            //Convert Milimeters to In
            if originalLength.selectedSegmentIndex == 4 && convertLength.selectedSegmentIndex == 0{
                let result = numString * 0.0393701;
                convertionResult.text = "\(result) Inches"
            }
            //Convert Milimeters to Ft
            if originalLength.selectedSegmentIndex == 4 && convertLength.selectedSegmentIndex == 1{
                let result = numString * 0.00328084;
                convertionResult.text = "\(result) Feet"
            }
            //Convert Milimeters to Yrd
            if originalLength.selectedSegmentIndex == 4 && convertLength.selectedSegmentIndex == 2{
                let result = numString * 0.00109361;
                convertionResult.text = "\(result) Yards"
            }
            //Convert Milimeters to Mi
            if originalLength.selectedSegmentIndex == 4 && convertLength.selectedSegmentIndex == 3{
                let result = numString * 6.2137e-7;
                convertionResult.text = "\(result) Miles"
            }
            //Convert Milimeters to cm
            if originalLength.selectedSegmentIndex == 4 && convertLength.selectedSegmentIndex == 5{
                let result = numString * 0.1;
                convertionResult.text = "\(result) Centimeters"
            }
            //Convert Milimeters to M
            if originalLength.selectedSegmentIndex == 4 && convertLength.selectedSegmentIndex == 6{
                let result = numString * 0.001;
                convertionResult.text = "\(result) Meters"
            }
            //Convert Milimeters to Km
            if originalLength.selectedSegmentIndex == 4 && convertLength.selectedSegmentIndex == 7{
                let result = numString * 1e-6;
                convertionResult.text = "\(result) Kilometers"
            }
            //ERROR Cannot convert Milimeters to Milimeters
            if originalLength.selectedSegmentIndex == 4 && convertLength.selectedSegmentIndex == 4{
                let alert = UIAlertController(title: "ERROR", message: "Cannot Convert Milimeters to Milimeters ", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
            }
            
            
            //Convert Centimeters to In
            if originalLength.selectedSegmentIndex == 5 && convertLength.selectedSegmentIndex == 0{
                let result = numString * 0.393701;
                convertionResult.text = "\(result) Inches"
            }
            //Convert Centimeters to Ft
            if originalLength.selectedSegmentIndex == 5 && convertLength.selectedSegmentIndex == 1{
                let result = numString * 0.0328084;
                convertionResult.text = "\(result) Feet"
            }
            //Convert Centimeters to Yrd
            if originalLength.selectedSegmentIndex == 5 && convertLength.selectedSegmentIndex == 2{
                let result = numString * 0.0109361;
                convertionResult.text = "\(result) Yards"
            }
            //Convert Centimeters to Mi
            if originalLength.selectedSegmentIndex == 5 && convertLength.selectedSegmentIndex == 3{
                let result = numString * 6.2137e-6;
                convertionResult.text = "\(result) Miles"
            }
            //Convert Centimeters to mm
            if originalLength.selectedSegmentIndex == 5 && convertLength.selectedSegmentIndex == 4{
                let result = numString * 10;
                convertionResult.text = "\(result) Milimeters"
            }
            //Convert Centimeters to M
            if originalLength.selectedSegmentIndex == 5 && convertLength.selectedSegmentIndex == 6{
                let result = numString * 0.01;
                convertionResult.text = "\(result) Meters"
            }
            //Convert Centimeters to Km
            if originalLength.selectedSegmentIndex == 5 && convertLength.selectedSegmentIndex == 7{
                let result = numString * 1e-5;
                convertionResult.text = "\(result) Kilometers"
            }
            //ERROR Cannot convert Centimets to Centimeters
            if originalLength.selectedSegmentIndex == 5 && convertLength.selectedSegmentIndex == 5{
                let alert = UIAlertController(title: "ERROR", message: "Cannot Convert Centimeters to Centimeters ", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
            }
            
            
            //Convert Meters to In
            if originalLength.selectedSegmentIndex == 6 && convertLength.selectedSegmentIndex == 0{
                let result = numString * 39.3701;
                convertionResult.text = "\(result) Inches"
            }
            //Convert Meters to Ft
            if originalLength.selectedSegmentIndex == 6 && convertLength.selectedSegmentIndex == 1{
                let result = numString * 3.28084;
                convertionResult.text = "\(result) Feet"
            }
            //Convert Meters to Yrd
            if originalLength.selectedSegmentIndex == 6 && convertLength.selectedSegmentIndex == 2{
                let result = numString * 1.09361;
                convertionResult.text = "\(result) Yards"
            }
            //Convert Meters to MI
            if originalLength.selectedSegmentIndex == 6 && convertLength.selectedSegmentIndex == 3{
                let result = numString * 0.000621371;
                convertionResult.text = "\(result) Miles"
            }
            //Convert Meters to mm
            if originalLength.selectedSegmentIndex == 6 && convertLength.selectedSegmentIndex == 4{
                let result = numString * 1000;
                convertionResult.text = "\(result) Milimeters"
            }
            //Convert Meters to cm
            if originalLength.selectedSegmentIndex == 6 && convertLength.selectedSegmentIndex == 5{
                let result = numString * 100;
                convertionResult.text = "\(result) Centimeters"
            }
            //Convert Meters to Km
            if originalLength.selectedSegmentIndex == 6 && convertLength.selectedSegmentIndex == 7{
                let result = numString * 0.001;
                convertionResult.text = "\(result) Kilometers"
            }
            //ERROR Cannot Convert Meters to Meters
            if originalLength.selectedSegmentIndex == 6 && convertLength.selectedSegmentIndex == 6{
                let alert = UIAlertController(title: "ERROR", message: "Cannot Convert Meters to Meters", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
            }
            
            
            //Convert Kilometers to In
            if originalLength.selectedSegmentIndex == 7 && convertLength.selectedSegmentIndex == 0{
                let result = numString * 39370.1;
                convertionResult.text = "\(result) Inches"
            }
            //Convert Kilometers to Ft
            if originalLength.selectedSegmentIndex == 7 && convertLength.selectedSegmentIndex == 1{
                let result = numString * 3280.84;
                convertionResult.text = "\(result) Feet"
            }
            //Convert Kilometers to Yrd
            if originalLength.selectedSegmentIndex == 7 && convertLength.selectedSegmentIndex == 2{
                let result = numString * 1093.61;
                convertionResult.text = "\(result) Yards"
            }
            //Convert Kilometers to Mi
            if originalLength.selectedSegmentIndex == 7 && convertLength.selectedSegmentIndex == 3{
                let result = numString * 0.621371;
                convertionResult.text = "\(result) Miles"
            }
            //Convert Kilometers to mm
            if originalLength.selectedSegmentIndex == 7 && convertLength.selectedSegmentIndex == 4{
                let result = numString * 1e+6;
                convertionResult.text = "\(result) Milimeters"
            }
            //Convert Kilometers to cm
            if originalLength.selectedSegmentIndex == 7 && convertLength.selectedSegmentIndex == 5{
                let result = numString * 100000;
                convertionResult.text = "\(result) Centimeters"
            }
            //Convert Kilometers to M
            if originalLength.selectedSegmentIndex == 7 && convertLength.selectedSegmentIndex == 6{
                let result = numString * 1000;
                convertionResult.text = "\(result) Meters"
            }
            //ERROR Cannot Convert Kilometers to Kilometers
            if originalLength.selectedSegmentIndex == 7 && convertLength.selectedSegmentIndex == 7{
                let alert = UIAlertController(title: "ERROR", message: "Cannot Convert Kilometers to Kilometers", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
            }
    }
    
    //Button to clear the text field and result label
    @IBAction func clearButton(sender: UIButton) {
        convertionResult.text = nil;
        numberInput.text = nil;
    }
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
