//
//  TemperatureViewController.swift
//  APD1_Roshan_M_1
//
//  Created by Roshan Mykoo on 11/6/15.
//  Copyright © 2015 Roshan Mykoo. All rights reserved.
//

import UIKit


class TemperatureViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var originalTemp: UISegmentedControl!
    @IBOutlet weak var convertedTemp: UISegmentedControl!
    @IBOutlet weak var numberInput: UITextField!
    @IBOutlet weak var conversionResult: UITextView!
    var numString : Double = 0.0
    var convertTemp : Double = 0.0;
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        //Setting the Second segmented control to the second option to prevent a same conversion error from starting when the app initally loads
        self.convertedTemp.selectedSegmentIndex = 1;
        self.numberInput.delegate = self;
        
        //Tap gesture to dissmiss the keyboard.
        let bye: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(bye)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    
    }
    
    
    //Dismiss Keyboard Function
    func dismissKeyboard() {
        view.endEditing(true);
    }
    

    
    @IBAction func convertButton(sender: UIButton) {
      //  var numberHolder = Double(numberInput.text)
        if let newNumb =  Double(numberInput.text!) {
            self.numString = newNumb;
        }
        
        //Converting Fagerenheit to Celsius
        if originalTemp.selectedSegmentIndex == 0 && convertedTemp.selectedSegmentIndex == 1{
            var numMinus: Double;
            var numMulti: Double;
            var numDiv: Double;
            numMinus = numString - 32.0;
            numMulti = numMinus * 5.0 ;
            numDiv = numMulti / 9.0;
            convertTemp = numDiv;
            let y = round(100.0 * convertTemp) / 100.0
            //print("Original Temp is \(numString) and converted temp is \(convertTemp)")
            conversionResult.text = "\(y)\u{00B0}C"
        }
        //Converting Celsius to Faherenheit
        if originalTemp.selectedSegmentIndex == 1 && convertedTemp.selectedSegmentIndex == 0{
            var numAdd: Double;
            var numMulti: Double;
            var numDiv: Double;
            numMulti = numString * 9;
              numDiv = numMulti / 5;
              numAdd = numDiv + 32;
            convertTemp = numAdd
            let y = round(100.0 * convertTemp) / 100.0
            //print("Original Temp is \(numString) and converted temp is \(convertTemp)")
            conversionResult.text = "\(y)\u{00B0}F"
        }
        
        //Error Faherenheit to Faherenheit
        if originalTemp.selectedSegmentIndex == 0 && convertedTemp.selectedSegmentIndex == 0{
            let alert = UIAlertController(title: "ERROR", message: "Cannot Convert Faherenheit to Faherenheit", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
        //Error Celsius to Celsius
        if originalTemp.selectedSegmentIndex == 1 && convertedTemp.selectedSegmentIndex == 1{
            let alert = UIAlertController(title: "ERROR", message: "Cannot Convert Celsius to Celsius", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
            }

    
    
    //Button to clear the text field and result label
    @IBAction func clearButton(sender: UIButton) {
        conversionResult.text = nil;
        numberInput.text = nil;
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
