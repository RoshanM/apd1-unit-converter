//
//  VolumeViewController.swift
//  APD1_Roshan_M_1
//
//  Created by Roshan Mykoo on 11/6/15.
//  Copyright © 2015 Roshan Mykoo. All rights reserved.
//

import UIKit

class VolumeViewController: UIViewController {
    @IBOutlet weak var originalUnit: UISegmentedControl!
    @IBOutlet weak var numberInput: UITextField!
    @IBOutlet weak var convertedUnit: UISegmentedControl!
    @IBOutlet weak var resultLabel: UITextView!
    var numString : Double = 0.0
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Setting the Second segmented control to the second option to prevent a same conversion error from starting when the app initally loads
        convertedUnit.selectedSegmentIndex = 1;
        
        // Do any additional setup after loading the view.
        
        //Tap gesture to dissmiss the keyboard.
        let bye: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(bye)
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //Dismiss Keyboard Function
    func dismissKeyboard() {
        view.endEditing(true);
    }
    
   
    @IBAction func convertButton(sender: UIButton) {
        if let newNumb =  Double(numberInput.text!) {
            self.numString = newNumb;
        }
        
        
        //Conveting Cups to Pints
        if originalUnit.selectedSegmentIndex == 0 && convertedUnit.selectedSegmentIndex == 1{
            let result = numString * 0.50721;
            resultLabel.text = "\(result) Pints"
        }
        //Conveting Cups to Quarts
        if originalUnit.selectedSegmentIndex == 0 && convertedUnit.selectedSegmentIndex == 2{
            let result = numString * 0.253605;
            resultLabel.text = "\(result) Quarts"
        }
        //Conveting Cups to Gallons
        if originalUnit.selectedSegmentIndex == 0 && convertedUnit.selectedSegmentIndex == 3{
            let result = numString * 0.0634013;
            resultLabel.text = "\(result) Gallons"
        }
        //Conveting Cups to Liters
        if originalUnit.selectedSegmentIndex == 0 && convertedUnit.selectedSegmentIndex == 4{
            let result = numString * 0.24;
            resultLabel.text = "\(result) Liters"
        }
        //ERROR Cups to Cups
        if originalUnit.selectedSegmentIndex == 0 && convertedUnit.selectedSegmentIndex == 0{
            let alert = UIAlertController(title: "ERROR", message: "Cannot Convert Cups to Cups", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
        
        
        //Converting Pints to Cups
        if originalUnit.selectedSegmentIndex == 1 && convertedUnit.selectedSegmentIndex == 0{
            let result = numString * 1.97157;
            resultLabel.text = "\(result) Cups"
        }
        //Converting Pints to Quarts
        if originalUnit.selectedSegmentIndex == 1 && convertedUnit.selectedSegmentIndex == 2{
            let result = numString * 0.5;
            resultLabel.text = "\(result) Quarts"
        }
        //Converting Pints to Gallons
        if originalUnit.selectedSegmentIndex == 1 && convertedUnit.selectedSegmentIndex == 3{
            let result = numString * 0.125;
            resultLabel.text = "\(result) Gallons"
        }
        //Converting Pints to Liters
        if originalUnit.selectedSegmentIndex == 1 && convertedUnit.selectedSegmentIndex == 4{
            let result = numString * 0.473176;
            resultLabel.text = "\(result) Liters"
        }
        //Error Pints to Pints
        if originalUnit.selectedSegmentIndex == 1 && convertedUnit.selectedSegmentIndex == 1{
            let alert = UIAlertController(title: "ERROR", message: "Cannot Convert Pints to Pints", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
        
        
        //Converting Quarts to Cups
        if originalUnit.selectedSegmentIndex == 2 && convertedUnit.selectedSegmentIndex == 0{
            let result = numString * 3.94314;
            resultLabel.text = "\(result) Cups"
        }
        //Converting Quarts to Pints
        if originalUnit.selectedSegmentIndex == 2 && convertedUnit.selectedSegmentIndex == 1{
            let result = numString * 2;
            resultLabel.text = "\(result) Pints"
        }
        //Converting Quarts to Gallons
        if originalUnit.selectedSegmentIndex == 2 && convertedUnit.selectedSegmentIndex == 3{
            let result = numString * 0.25;
            resultLabel.text = "\(result) Gallons"
        }
        //Converting Quarts to Liters
        if originalUnit.selectedSegmentIndex == 2 && convertedUnit.selectedSegmentIndex == 4{
            let result = numString * 0.946353;
            resultLabel.text = "\(result) Liters"
        }
        //ERROR Quarts to Quarts
        if originalUnit.selectedSegmentIndex == 2 && convertedUnit.selectedSegmentIndex == 2{
            let alert = UIAlertController(title: "ERROR", message: "Cannot Convert Quarts to Quarts", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
        
        
        //Converting Gallons to Cups
        if originalUnit.selectedSegmentIndex == 3 && convertedUnit.selectedSegmentIndex == 0{
            let result = numString * 15.7725;
            resultLabel.text = "\(result) Cups"
        }
        //Converting Gallons to Pints
        if originalUnit.selectedSegmentIndex == 3 && convertedUnit.selectedSegmentIndex == 1{
            let result = numString * 8;
            resultLabel.text = "\(result) Pints"
        }
        //Converting Gallons to Quarts
        if originalUnit.selectedSegmentIndex == 3 && convertedUnit.selectedSegmentIndex == 2{
            let result = numString * 4;
            resultLabel.text = "\(result) Quarts"
        }
        //Converting Gallons to Liters
        if originalUnit.selectedSegmentIndex == 3 && convertedUnit.selectedSegmentIndex == 4{
            let result = numString * 3.78541;
            resultLabel.text = "\(result) Liters"
        }
        //ERROR Gallons to Gallons
        if originalUnit.selectedSegmentIndex == 3 && convertedUnit.selectedSegmentIndex == 3{
            let alert = UIAlertController(title: "ERROR", message: "Cannot Convert Gallons to Gallons", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
        
        
        //Converting Liters to Cups
        if originalUnit.selectedSegmentIndex == 4 && convertedUnit.selectedSegmentIndex == 0{
            let result = numString * 4.16667;
            resultLabel.text = "\(result) Cups"
        }
        //Converting Liters to Pints
        if originalUnit.selectedSegmentIndex == 4 && convertedUnit.selectedSegmentIndex == 1{
            let result = numString * 2.11338;
            resultLabel.text = "\(result) Pints"
        }
        //Converting Liters to Quarts
        if originalUnit.selectedSegmentIndex == 4 && convertedUnit.selectedSegmentIndex == 2{
            let result = numString * 1.05669;
            resultLabel.text = "\(result) Quarts"
        }
        //Converting Liters to Gallons
        if originalUnit.selectedSegmentIndex == 4 && convertedUnit.selectedSegmentIndex == 3{
            let result = numString * 0.264172;
            resultLabel.text = "\(result) Gallons"
        
        }
        //ERROR Liters to Liters
        if originalUnit.selectedSegmentIndex == 4 && convertedUnit.selectedSegmentIndex == 4{
            let alert = UIAlertController(title: "ERROR", message: "Cannot Convert Liters to Liters", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    //Button to clear the text field and result label
    @IBAction func clearButton(sender: AnyObject) {
        resultLabel.text = nil;
        numberInput.text = nil
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
