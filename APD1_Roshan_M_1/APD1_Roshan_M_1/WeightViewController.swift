//
//  WeightViewController.swift
//  APD1_Roshan_M_1
//
//  Created by Roshan Mykoo on 11/6/15.
//  Copyright © 2015 Roshan Mykoo. All rights reserved.
//

import UIKit

class WeightViewController: UIViewController {
    @IBOutlet weak var originalWeight: UISegmentedControl!
    @IBOutlet weak var convertedWeight: UISegmentedControl!
    @IBOutlet weak var numberInput: UITextField!
    @IBOutlet weak var conversionResult: UITextView!
    var numString : Double = 0.0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Setting the Second segmented control to the second option to prevent a same conversion error from starting when the app initally loads
        convertedWeight.selectedSegmentIndex = 1;

        // Do any additional setup after loading the view.
        
        //Tap gesture to dissmiss the keyboard.
        let bye: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(bye)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //Dismiss Keyboard Function
    func dismissKeyboard() {
        view.endEditing(true);
    }
    
    @IBAction func convertButton(sender: UIButton) {
        if let newNumb =  Double(numberInput.text!) {
            self.numString = newNumb;
        }
      //  let ogWeight = NSString(string: numberInput.text!).doubleValue
        
        
        //Converting Ounces to Kilograms
        if originalWeight.selectedSegmentIndex == 0 && convertedWeight.selectedSegmentIndex == 3{
            let result = numString / 1000;
            conversionResult.text = "\(result) Kilograms"
        }
        //Converting Ounces to Grams
        if originalWeight.selectedSegmentIndex == 0 && convertedWeight.selectedSegmentIndex == 2{
            let result = numString * 28.3495
            conversionResult.text = "\(result) Grams"
        }
        
        //Converting Ounces to Pounds
        if originalWeight.selectedSegmentIndex == 0 && convertedWeight.selectedSegmentIndex == 1{
            let result = numString / 16;
            conversionResult.text = "\(result) Pounds"
        }
        //ERROR Ounces to Ounces
        if originalWeight.selectedSegmentIndex == 0 && convertedWeight.selectedSegmentIndex == 0{
            let alert = UIAlertController(title: "ERROR", message: "Cannot Convert Ounces to Ounces", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
        
        
        //Converting Pounds to Kilograms
        if originalWeight.selectedSegmentIndex == 1 && convertedWeight.selectedSegmentIndex == 3{
            let result = numString * 0.4536;
            conversionResult.text = "\(result) Kilograms"
        }
        //Converting Pounds to Grams
        if originalWeight.selectedSegmentIndex == 1 && convertedWeight.selectedSegmentIndex == 2{
            let result = numString * 453.592;
            conversionResult.text = "\(result) Grams"
        }
        //Converting Pounds to Ounces
        if originalWeight.selectedSegmentIndex == 1 && convertedWeight.selectedSegmentIndex == 0{
            let result = numString / 16;
            conversionResult.text = "\(result) Ounces"
        }
        //ERROR Pounds to Pounds
        if originalWeight.selectedSegmentIndex == 1 && convertedWeight.selectedSegmentIndex == 1{
            let alert = UIAlertController(title: "ERROR", message: "Cannot Convert Pounds to Pounds", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
        
        
        //Converting Grams to Kilograms
        if originalWeight.selectedSegmentIndex == 2 && convertedWeight.selectedSegmentIndex == 3{
            let result = numString * 0.001;
            conversionResult.text = "\(result) Kilograms"
        }
        //Converting Grams to Pounds
        if originalWeight.selectedSegmentIndex == 2 && convertedWeight.selectedSegmentIndex == 1{
            let result = numString * 0.00220462;
            conversionResult.text = "\(result) Pounds"
        }
        //Converting Grams to Ounces
        if originalWeight.selectedSegmentIndex == 2 && convertedWeight.selectedSegmentIndex == 0{
            let result = numString * 0.035274;
            conversionResult.text = "\(result) Ounces"
        }
        //ERROR Grams to Grams
        if originalWeight.selectedSegmentIndex == 2 && convertedWeight.selectedSegmentIndex == 2{
            let alert = UIAlertController(title: "ERROR", message: "Cannot Convert Grams to grams", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
        
        
        //Converting Kilograms to Ounces
        if originalWeight.selectedSegmentIndex == 3 && convertedWeight.selectedSegmentIndex == 0{
            let result = numString * 35.274;
            conversionResult.text = "\(result) Ounces"
        }
        //Converting Kilograms to Pounds
        if originalWeight.selectedSegmentIndex == 3 && convertedWeight.selectedSegmentIndex == 1{
            let result = numString * 2.20462;
            conversionResult.text = "\(result) Pounds"
        }
        //Converting Kilograms to Grams
        if originalWeight.selectedSegmentIndex == 3 && convertedWeight.selectedSegmentIndex == 2{
            let result = numString * 1000;
            conversionResult.text = "\(result) Grams"
        }
        //ERROR Kilograms to Kilograms
        if originalWeight.selectedSegmentIndex == 3 && convertedWeight.selectedSegmentIndex == 3{
            let alert = UIAlertController(title: "ERROR", message: "Cannot Convert Kilograms to Kilograms", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }

    
    
    //Button to clear the text field and result label
    @IBAction func clearButton(sender: UIButton) {
        conversionResult.text = nil;
        numberInput.text = nil;
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
